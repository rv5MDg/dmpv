# dMPV
Wrapper for MPV player adding synchronization and chat capabilities.
This project uses OpenGL to render the video via a framebuffer and text (danmaku) using a custom font renderer and matrix transformation in the shader.

We sync up messages locally with a python interface via TCP sockets.
The interface retreives messages from an online plateform, using Websocket for instance.

VERY UNSTABLE
